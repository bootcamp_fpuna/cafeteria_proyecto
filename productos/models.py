from django.db import models

class Producto(models.Model):
    nombre = models.CharField(max_length=100)
    precio = models.IntegerField(default=0)
    imagen =models.ImageField(upload_to="imagenes", null=True)

    def __str__(self) -> str:
        return self.nombre

