**Proyecto Cafeteria**

**Herramientas instaladas para el proyecto backend + frontend:**

- Visual Studio code 
- python: v3.11.2
- Mongod: v6.0.5 
- Node: v18.12.1 
    

**Clonar el repositorio del proyecto:** 

- git clone https://gitlab.com/bootcamp_fpuna/cafeteria_proyecto.git 


**Crear entorno virtual de python y luego activarlo:**

- python -m venv venv 
- env\Scripts\activate.bat 
    

**Actualizar pip**

- python -m pip install --upgrade pip 
    

**Instalar las dependencias necesarias:**
- pip install -r requirements.txt


**Para crear la conexion con la base de datos configurado en archivo Settings, realizar las migraciones necesarias:**
- python .\manage.py makemigrations
- python .\manage.py migrate


**crear super usuario admin para acceder a administrador de django:**
- python .\manage.py createsuperuser


**Iniciar servidor del backend:**
- python .\manage.py runserver


**Dirigirse a http://127.0.0.1:8000/, desde Admin de djnago crear grupos:**
- recepcion
- cocina


**Crear usuarios:**
- Recepcionista
- Cocinero
a estos usuarios asignaremos cada grupo/rol correspondiente:


Tambien cargamos los productos desde el Admin de Django, las imagenes se encuentran en la carpeta media/imagenes


**Iniciar servidor del frontend:**
- npm run dev
    
    
**Abrir app Cafeteria en: http://localhost:5173/**
