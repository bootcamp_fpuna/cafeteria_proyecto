import React, { useState, useEffect } from 'react';

const VerPedido = () => {
  const [pedidos, setPedidos] = useState([]);
  const [productos, setProductos] = useState([]);
  const [tiempos, setTiempos] = useState({});

  const formatoHora = (timeString) => {
    const options = { hour: 'numeric', minute: 'numeric', second: 'numeric' };
    const formatter = new Intl.DateTimeFormat(undefined, options);
    const time = new Date(timeString);
    const formattedTime = formatter.format(time);
    return formattedTime;
  };

  const formatoTiempo = (timeString) => {
    const time = new Date(timeString);
    const minutes = time.getMinutes().toString().padStart(2, '0');
    const seconds = time.getSeconds().toString().padStart(2, '0');
    return `${minutes}:${seconds}`;
  };

  const actualizarTiempos = () => {
    setTiempos((prevTiempos) => {
      const nuevosTiempos = {};

      for (const pedido of pedidos) {
        const tiempoTranscurrido = Date.now() - new Date(pedido.fecha_pedido).getTime();
        nuevosTiempos[pedido.id] = tiempoTranscurrido;
      }

      return nuevosTiempos;
    });
  };


  useEffect(() => {
    fetch('http://localhost:8000/pedidos/', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${JSON.parse(
          window.localStorage.getItem('accessToken')
        )}`,
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setPedidos(data);
      });
  }, []);


  useEffect(() => {
    pedidos.forEach((pedido) => {
      fetch(`http://127.0.0.1:8000/pedidos/${pedido.id}/productos/`, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${JSON.parse(
            window.localStorage.getItem('accessToken')
          )}`,
          'Content-Type': 'application/json',
        },
      })
        .then((response) => response.json())
        .then((data) => {
          setProductos((prevProductos) => ({
            ...prevProductos,
            [pedido.id]: data,
          }));
        })
        .catch((error) => console.error('Error:', error));
    });
  }, [pedidos]);


  useEffect(() => {
    const intervalId = setInterval(actualizarTiempos, 1000); // Actualiza los tiempos cada segundo

    return () => {
      clearInterval(intervalId); // Limpia el intervalo al desmontar el componente
    };
  }, );



  // Filtrar pedidos por estado 'pendiente'
  const pedidosPendientes = pedidos.filter((pedido) => pedido.estado === 'pendiente');

  const handleUpdatePedido = (pedidoId, cli, nummesa, lista_pro) => {
    const updatedPedidos = pedidos.map((pedido) => {
      if (pedido.id === pedidoId) {
        return { 
          ...pedido, 
          estado: 'listo',
          fecha_pedido_listo: new Date().toISOString(),
        }; // Actualizar el estado del pedido
      }
      return pedido;
    });

    fetch(`http://localhost:8000/pedidos/${pedidoId}/`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${JSON.parse(
          window.localStorage.getItem('accessToken')
        )}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        estado: 'Listo',
        cliente: cli,
        mesa: nummesa,
        lista_productos: lista_pro,
        fecha_pedido_listo: new Date().toISOString(),
      }), // Actualizar el estado en el cuerpo de la solicitud
    })
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else {
          throw new Error('Error al actualizar el pedido');
        }
      })
      .then((data) => {
        setPedidos(updatedPedidos); // Actualizar el estado local de los pedidos
      })
      .catch((error) => {
        console.error('Error al actualizar el pedido:', error);
      });
  };


  return (
    <>
      <h2 className="titulo-pedidos">Pedidos y Productos</h2>
      <div className="lista-cocinero">
      {pedidosPendientes.length === 0 ? (
        <p>NO EXISTEN PEDIDOS PENDIENTES</p>
      ) : (
        pedidosPendientes.map((pedido) => (
          <div className="tarjeta-cocinero"  key={pedido.id}>
            <h4 className="t4">Pedido #{pedido.id}</h4>
            <h4  className="t5">Cliente: {pedido.cliente}</h4 >
            <h4 className="t5">Mesa: {pedido.mesa}</h4>
            <h4 className="t5">Hora pedido: {formatoHora(pedido.fecha_pedido)}</h4>
            {/* <h4 className="t5">Tiempo transcurrido: {formatoHora(tiempos[pedido.id] || 0)}</h4> */}
            <h4 className="t5">Tiempo transcurrido: {formatoTiempo(tiempos[pedido.id] || 0)}</h4>
            
            <h4 className='letra-cocinero'>Productos</h4>
              {Array.isArray(productos[pedido.id]) ? (
                productos[pedido.id].map((producto) => (
                  <div className="letra-prod" key={producto.id}>
                    <h4 className="letra-prod">{producto.nombre}</h4>
                  </div>
                ))
              ) : null}


            <h4 className="letra-estado">Estado: {pedido.estado}</h4>
            {pedido.estado !== 'Listo' && (
              // Mostrar el botón de "Listo" solo si el estado no es "Listo"
              <button className="boton-cocina"
                onClick={() =>
                  handleUpdatePedido(
                    pedido.id,
                    pedido.cliente,
                    pedido.mesa,
                    JSON.parse(pedido.lista_productos) // Convertir la lista de productos de JSON a una lista
                  )
                }
              >
                Listo
              </button>
            )} 
          </div>
        ))
      )}
      </div>
    </>
  );
};

export default VerPedido;
