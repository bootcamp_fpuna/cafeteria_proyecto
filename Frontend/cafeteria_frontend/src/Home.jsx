import React, { useState, useEffect } from 'react';
import TomarPedido from './TomarPedido';
import VerPedido from './VerPedidos';
import './index.css';
import coffeeIcon from './assets/coffee.png';
import EntregarPedido from './EntregarPedidos';
import Notificaciones from './Notificaciones';



const Home = ({ onLogout, userId }) => {
  const [user, setUser] = useState();
  const [isActive, setActive] = useState("false");

  const handleToggle = () => {
    setActive(!isActive);
  };

  useEffect(() => {
    fetch('http://localhost:8000/users/' + userId, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${JSON.parse(
          window.localStorage.getItem('accessToken')
        )}`,
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((userData) => {
        setUser(userData);
      });
  }, []);

  const logoutHandler = () => {
    onLogout();
  };

  const role = user ? user.group_name : null;

  const content = role && role === 'recepcion' && isActive ? <TomarPedido /> : <VerPedido />;
  // const content2 = role && role === 'cocina' ? <VerPedido /> : <p>Home Page</p>;
  
  const content3 = <EntregarPedido />
  const notifica = role && role === 'recepcion' ? <Notificaciones /> : <VerPedido />;

  // const content = () => {
  //   if (role && role === 'recepcion') {
  //     if (isActive) {
  //       return <EntregarPedido />; } 
  //     else {
  //       return <TomarPedido />;
  //     } 
  //   } 
  //   else if (role && role === 'cocina') {
  //     {
  //       return <VerPedidos />;
  //     }
  //   }
  // };

  return (

  <>
    <div className="home">

     <nav className="navbar">
      <div className="col-navbar-1">
        <img src={coffeeIcon} className="imagen-barra" alt="Coffee Icon" />
        <h3 className="nombre">Coffee House</h3>

      </div>
      <div className="col-navbar-2">
        {role && role === 'recepcion' ? 
          (isActive ?
            (<abbr title="Ver Pedidos">
                <svg onClick={handleToggle} xmlns="http://www.w3.org/2000/svg" className="boton-lista" viewBox="0 0 16 16">
                  <path d="M12 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM5 4h6a.5.5 0 0 1 0 1H5a.5.5 0 0 1 0-1zm-.5 2.5A.5.5 0 0 1 5 6h6a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5zM5 8h6a.5.5 0 0 1 0 1H5a.5.5 0 0 1 0-1zm0 2h3a.5.5 0 0 1 0 1H5a.5.5 0 0 1 0-1z"/>
                </svg>
             </abbr>) :
            (<abbr title="Tomar Pedido">
              <svg onClick={handleToggle}  xmlns="http://www.w3.org/2000/svg" className="boton-lista" viewBox="0 0 16 16">
                <path d="M8.707 1.5a1 1 0 0 0-1.414 0L.646 8.146a.5.5 0 0 0 .708.708L8 2.207l6.646 6.647a.5.5 0 0 0 .708-.708L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.707 1.5Z"/>
                <path d="m8 3.293 6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6Z"/>
              </svg>
             </abbr>)) : 
          <h3 className="nombre"></h3>}
        { }
        <abbr title="Cerrar Sesion">
          <svg onClick={logoutHandler} className="boton-cerrar"
          xmlns="http://www.w3.org/2000/svg"   viewBox="0 0 16 16">
            <path d="M12 1a1 1 0 0 1 1 1v13h1.5a.5.5 0 0 1 0 1h-13a.5.5 0 0 1 0-1H3V2a1 1 0 0 1 1-1h8zm-2 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
          </svg>
        </abbr>
        </div>  
    </nav> 
   {user && (
      <>
        <h3 className="saludo" >{user.username}</h3>
        {/* <h3 className="rol">Rol: {user.group_name}</h3> */}
        <div className="contenedor">
          {/* si el boton esta activo que me muestre los pedidos, 
          de lo contrario me muestra para realizar los pedidos */}
          {isActive ? content : content3 }
          {/* {user.group_name === 'cocina' ? content2 : <p>prueba</p>} */}
          {/* {content2} */}
        </div>
      </> 
    )}
</div>
</>
  );
};

export default Home;

