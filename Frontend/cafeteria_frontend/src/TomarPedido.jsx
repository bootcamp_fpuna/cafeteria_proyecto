
import React, { useState, useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useNotificationCenter } from 'react-toastify/addons/use-notification-center';



const TomarPedido = () => {
  const [productos, setProductos] = useState([]);
  const [seleccionados, setSeleccionados] = useState([]);
  const [totalPrecio, setTotalPrecio] = useState(0);
  const [numMesa, setNumMesa] = useState('');
  const [cliente, setCliente] = useState('');
  const [pedidoEnviado, setPedidoEnviado] = useState(false);

  useEffect(() => {
    fetch('http://localhost:8000/productos/', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${JSON.parse(
          window.localStorage.getItem('accessToken')
        )}`,
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setProductos(data);
      });
  }, []);

  const agregarSeleccionado = (producto) => {
    setSeleccionados([...seleccionados, producto]);
    setTotalPrecio(totalPrecio + producto.precio);
  };

  const eliminarSeleccionado = (producto) => {
    const index = seleccionados.indexOf(producto);
    if (index > -1) {
      const nuevosSeleccionados = [...seleccionados];
      nuevosSeleccionados.splice(index, 1);
      setSeleccionados(nuevosSeleccionados);
      setTotalPrecio(totalPrecio - producto.precio);
    }
  };

  const guardarPedido = () => {
    console.log(cliente.trim());
    if (cliente.trim() === '') {
      toast.error('Por favor, ingrese el nombre del cliente.'
      , {
        position: toast.POSITION.TOP_CENTER
      });
     // toast.error('Error Notification !'

      return;
    }
  
    if (isNaN(numMesa) || numMesa <= 0) {
      toast.error('Por favor, ingrese un número de mesa válido.');
      return;
    }
  
    const lista_pro = seleccionados.map((producto) => producto.id);
    fetch('http://localhost:8000/pedidos/', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${JSON.parse(
          window.localStorage.getItem('accessToken')
        )}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        mesa: numMesa,
        cliente: cliente,
        lista_productos: lista_pro,
      }),
    })
      .then((res) => {
        if (res.status === 201) {
          setPedidoEnviado(true);
          toast.success('Pedido enviado');
          setNumMesa('');
          setCliente('');
          setSeleccionados([]);
          setTotalPrecio(0);
        } else {
          setPedidoEnviado(true);
          toast.error('No se pudo enviar el pedido. Por favor, complete todos los datos.');
        }
        return res.json();
      })
      .then((data) => {
        console.log(data);
      });
  };
  

  return (
    <>
       <div className="fila">
         <div className="columna-wrap">
          <h2 className="h4">Pedido</h2> 
          <label htmlFor="numMesa" className="h4">Nro Mesa:</label>
          <input
            id="numMesa"
            type="number"
            className="mesa"
            // min={1}
            value={numMesa}
            onChange={(e) => setNumMesa(e.target.value)}
          />
        </div>   
         <div className="columna-wrap">
            <label htmlFor="cliente" className="h4">Cliente:</label>
            <input
              id="cliente"
              type="text"
              className="input-cliente"
              placeholder="Nombre"
              value={cliente}
              onChange={(e) => setCliente(e.target.value)}
            />
        </div> 
      </div> 
      <div className="fila-central">
        <div className="columna">
          <h3 className="h3">Productos</h3>
          <div className="fila-wrap"> 
            {productos.map((producto) => (
           <div className="tarjeta" key={producto.id}>
           
              <img src= {producto.imagen} className="imagen-menu" alt="producto"/>
              
                <h5 className="h5">{producto.nombre}</h5>
                <h5 className="h5">Precio: {producto.precio}</h5>
                  <button onClick={() => agregarSeleccionado(producto)} className="boton-agregar" value="Agregar">
                    Agregar
                    <svg xmlns="http://www.w3.org/2000/svg" className="add"
                        viewBox="0 0 16 16">
                      <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3a.5.5 0 0 1 1 0z"/>
                    </svg>
                  </button>
           
           </div>
          ))} 
          </div> 
        </div>
        <div className="columna">
          <h3 className="h3">Seleccionados</h3>
          <div className="lista">
              <ul className="ul">
                {seleccionados.map((producto) => {            
                  return (
                    
                    <li key={producto.id} className="li">
                      {producto.nombre} - {producto.precio}
                      <button className="transparente" onClick={() => eliminarSeleccionado(producto)}>
                        <svg xmlns="http://www.w3.org/2000/svg"  className="remove" viewBox="0 0 16 16">
                          <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z"/>
                        </svg>
                    </button>
                    </li>
                  );
                })}
              </ul>
            </div>
        </div>
      </div>
      <div className="fila-precio">
        <h3 className="precio">Total Precio: {totalPrecio}</h3>
        <button onClick={guardarPedido} className="enviar">Enviar Pedido</button>
      </div>
      {pedidoEnviado && <ToastContainer /> }
      <ToastContainer />
       
    </>
  );
};

export default TomarPedido;



