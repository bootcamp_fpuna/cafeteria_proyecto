import { useState } from 'react'
import coffeeIcon from './assets/coffee.png'
import jwtDecode from 'jwt-decode'
import './index.css'


//Componente Login que recibe como prop del componente padre App el disparador de evento onLogin que ejecutara
//la funcion onLoginHandler
//Ademas este componente tiene 2 estados iniciales, username vacio y password vacio, para cambiar sus estados se 
//hara uso de setUsername y setPassword
const Login = ({ onLogin }) => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  //funcion manejadora del evento onSubmit
  const loginHandle = (e) => {
    e.preventDefault()
    // login and get an user with JWT token
    fetch('http://localhost:8000/api/token/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        username,
        password,
      }),
    })

      .then((res) => res.json())
      .then((tokenData) => {
        window.localStorage.setItem('accessToken', JSON.stringify(tokenData.access))
        console.log(tokenData);
        console.log(jwtDecode(tokenData.access).user_id);
        onLogin(jwtDecode(tokenData.access).user_id)
      })
  }

  //El componente renderizara un form que tendra el disparador de evento onSubmit y el cual dispara la ejecucion 
  //de la funcion loginHandle en caso que el usuario de click en Login luego de haber rellenado su usuario y contraseña
  return (
    <form onSubmit={loginHandle}>
    <div className="titulo">
      <img className="img" src={coffeeIcon} alt="Coffee Icon" />
      {/* <h1>Coffee House</h1> */}
    </div>
    <h2 className="center">Inicia Sesión</h2>
    <div className="input-fila">
      <input
        className="input borde-derecho"
        aria-label="Usuario"
        placeholder="Usuario"
        id="username"
        type="text"
        onChange={(e) => {
          setUsername(e.target.value)
        }}
      />        
      {/* icono copiado de Bootstrap */}
      <span className="input-icono">
        <svg xmlns="http://www.w3.org/2000/svg" className="icono-login" viewBox="0 0 16 16">
          <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3Zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6Z"/>
        </svg>
      </span>  
    </div>
    <div className="input-fila"> 
     <input
      className=" input borde-derecho"
      aria-label="Contraseña"
      placeholder="Contraseña"
      id="password"
      type="password"
      onChange={(e) => {
        setPassword(e.target.value)
      }}  
     /> 
      {/* icono copiado de Bootstrap */}
     <span className="input-icono">
        <svg xmlns="http://www.w3.org/2000/svg" className="icono-login" viewBox="0 0 16 16">
          <path d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
        </svg>          
      </span>
    </div>
    <button className="button" >Login</button> 
    <br></br>
    <a href="http://localhost:8000/admin/" className="h5">Iniciar sesion como admin, ingrese aqui</a>
    
</form> 
  )
}

export default Login