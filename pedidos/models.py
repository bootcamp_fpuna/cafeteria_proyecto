from django.db import models
from django.utils import timezone
import datetime


def get_hora_actual():
    return timezone.localtime(timezone.now()).time()

class Pedido(models.Model):
    cliente = models.CharField(max_length=100)
    mesa = models.IntegerField()
    lista_productos = models.JSONField()
    estado = models.CharField(max_length=20, default='pendiente')
    fecha_pedido = models.DateTimeField(auto_now_add=True)
    fecha_pedido_listo = models.DateTimeField(null=True, blank=True)
    ##fecha_listo = models.CharField(max_length=8, default=datetime.datetime.now().strftime('%H:%M:%S'))

    def save(self, *args, **kwargs):
        if self.estado == 'listo' and not self.fecha_pedido_listo:
            self.fecha_pedido_listo = timezone.now()
        super().save(*args, **kwargs)
        
    def __str__(self) -> str:
        return str(self.mesa)