from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from .models import Pedido
from .serializers import PedidoSerializer
from productos.models import Producto
from productos.serializers import ProductoSerializer
from cafeteria_proyecto.permissions import IsCocinero, IsRecepcionista, IsRecepcionistaOrCocinero


class PedidosViewSet(viewsets.ModelViewSet):
    queryset = Pedido.objects.all()
    serializer_class = PedidoSerializer

    def get_permissions(self):
        permission_classes = []
        if self.action == 'retrieve' or self.action == 'list':
            permission_classes = [IsRecepcionistaOrCocinero]
        elif self.action == 'create' or self.action == 'update' or self.action == 'partial_update' or self.action == 'destroy':
            permission_classes = [IsRecepcionistaOrCocinero]
        elif self.action == 'productos': # Endpoint custom
            permission_classes = [IsRecepcionistaOrCocinero]
        return [permission() for permission in permission_classes]

    @action(detail=True, methods=['get']) #redefinimos el endpoint que queremos, para obtener los productos de un pedido
    def productos(self, request, pk=None):
        id_pedido = self.kwargs['pk'] #obtenemos el pk del pedido desde la url del request
        try:
            pedido = Pedido.objects.get(pk=id_pedido) #obtenemos el pedido especifico
            lista_productos = []
            for producto_id in pedido.lista_productos:
                lista_productos.append(Producto.objects.get(pk=producto_id)) #cargamos a mano los productos obtenidos con su pk
            return Response(ProductoSerializer(lista_productos, many=True).data,    #serializamos el objeto python lista_productos a
                            status=status.HTTP_200_OK)                              #json para poder enviar la respuesta
        except Exception as e:
            print(e)
            return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
